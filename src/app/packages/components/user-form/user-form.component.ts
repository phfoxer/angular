import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  title = 'Cadastro de usuário';
  action = 'Adicionar';

  userForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      nome: ['', 
        [Validators.required, Validators.pattern(/[a-zA-Z\u00C0-\u00FF ]+/i)]
      ],
      email: ['', 
      [Validators.required, Validators.email]
      ],
      senha: ['', 
        [Validators.required, Validators.maxLength(24), Validators.minLength(8)]
      ],
      confirmarSenha: ['', Validators.required]
    });

    this.userForm.valueChanges
    .subscribe(
      form => this.validaForm()
    );
  }

  private validaForm(){
    // console.log(this.userForm.controls.nome.value);
    for(const key in this.formErrors){
       const campo = this.userForm.get(key);
       this.formErrors[key]['msg'] = '';//zerar mensagem de erro
       if(campo && campo.dirty && !campo.valid){
          for(const erro in campo.errors){
            console.log(this.formErrors[key][erro]);
            this.formErrors[key]['msg'] += this.formErrors[key][erro]+'<br>';
          }
       }
    }
  }

  formErrors = {
    'nome': {
      'required': 'Campo obrigatório',
      'pattern': 'Caracteres inválidos',
      'msg': ''
    },
    'email': {
      'required': 'Campo obrigatório',
      'email': 'O endereço de email não é válido',
      'msg': ''
    },
    'senha': {
      'required':      'Campo obrigatório',
      'minlength':     'A senha deve ter no mínimo 8 caracteres',
      'maxlength':     'A senha não pode ter mais de 24 caracteres',
      'msg': ''
    },
    'confirmarSenha': {
      'required': 'As senhas devem ser iguais',
      'senhasConferem': 'As senhas não conferem',
      'msg': ''
    }
  };

}
